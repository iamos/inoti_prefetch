import matplotlib.pyplot as plt
import sys
import numpy as np

file1 = open("/home/iamos/Documents/inoti/civ_cold_tp","r")
file2 = open("/home/iamos/Documents/inoti/civ_pref_tp","r")

x1 = []
while True:
	line = file1.readline().rstrip('\n')
	if not line : break
	terms = line.split()
	x1.append(terms[1])

x2 = []
while True:
	line = file2.readline().rstrip('\n')
	if not line : break
	terms = line.split()
	x2.append(terms[1])

N = 30
x_time = np.arange(0,N,1)

# plt.rcdefaults()
fig, ax = plt.subplots()
# plt.style.use('ggplot')
width = 0.4
#Field
ax.plot(x_time, x1, '.--', color='black', label='Prefetch')
# ax.plot(x_time, x1, '*', color='black')
ax.plot(x_time, x2, 'x-' , color='black', label='Coldstart')
# ax.plot(x_time, x2, 'x', color='black')
ax.legend()
#Text
plt.xlabel('Time (sec)')
plt.ylabel('Throughput (MB/s)')
#plt.title('Throughput per sec')
plt.show()