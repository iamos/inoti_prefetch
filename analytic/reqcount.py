# reqcount.py
import sys
import operator
import math

# print sys.argv
file = open("{}".format(sys.argv[1]),"r")

count = {}
totcount = 0
while True:
	line = file.readline().rstrip('\n')
	if not line : break
	terms = line.split()
	if terms[0] == 'CPU0' : break
	if terms[6] == ('R' or 'RM') and terms[5] == 'I':
		# print terms
		index = float(terms[3])//1
		totcount += 1
		if index not in count:
			count[index] = 1
		else:
			count[index] += 1

hashkeys = count.keys()
# print hashkeys
for x in range(0, int(max(hashkeys))+1):
	if  x in count:
		val = count[x]
	else:
		val = 0
	print "{:>4}\t {:8} ".format(x, val)
		
print "{:>4}\t {:8} ".format('total', totcount)

#1 sector = 512 bytes