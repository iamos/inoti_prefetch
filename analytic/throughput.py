# throughput.py
import sys
import operator
import math

# print sys.argv
file = open("{}".format(sys.argv[1]),"r")

throughput = {}
while True:
	line = file.readline().rstrip('\n')
	if not line : break
	terms = line.split()
	if terms[0] == 'CPU0' : break
	# ['8,5', '0', '23611', '14.473865839', '3808', 'I', 'R', '424501056', '+', '256', '[fgfs]']
	if terms[6] == ('R' or 'RM') and terms[5] == 'C':
		index = float(terms[3])//1
		size = int(terms[9])
		# print index, size
		if index not in throughput:
			throughput[index] = size
		else:
			throughput[index] += size

hashkeys = throughput.keys()
# print hashkeys
for x in range(0, int(max(hashkeys))+1):
	if x in throughput:
		tpval = (throughput[x]*512)/ (1024.0*1024)
	else:
		tpval = 0
	print "{:>4}\t {:7.3f}".format(x, tpval)

# for x in throughput:
# 	print "{:>4}\t {:7.3f} MB/s".format(x, (throughput[x]*512)/ (1024.0*1024))

#1 sector = 512 bytes