#define _GNU_SOURCE
#include <stdio.h>
#include <sys/inotify.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "prefetch.h"

#define NUM_OF_WATCH	4
#define FADVISE_LIMIT	131072
#define SEC_TO_NANO 1000000000LL
#define NANO_TO_MIRCO 0.001

void* do_prefetch(void* list) {
	struct fileinfolist* filelist = (struct fileinfolist*)list;
	struct fileinfo* node = filelist->head;
	int count = 0;
	while( node != NULL){
		// printf("%s -> %p\n", node->filepath, node->next);
		int fd = open(node->filepath, O_RDONLY);
		off_t offset = node->offset;
		off_t length = node->length;
		while(1){
			if( length > FADVISE_LIMIT){
				if( posix_fadvise(fd, offset, FADVISE_LIMIT, POSIX_FADV_WILLNEED) < 0 ){
					perror("posix_fadvise");
				}
				offset = offset + FADVISE_LIMIT;
				length = length - FADVISE_LIMIT;
			}
			else{
				if ( posix_fadvise(fd, offset, length, POSIX_FADV_WILLNEED) < 0){
					perror("posix_fadvise");
				}
				break;
			}
		}
		close(fd);
		node = node->next;
		count++;
	}
	printf("@thread prefetch %d files.\n", count);
	pthread_exit((void*)0);
	return NULL;
}

void print_procio(){
	// char procio[255];
	char* procio = "/proc/self/io";
	// memset(procio, '\0', 255);
	char buffer[4096];
	memset(buffer, '\0', 4096);

	// sprintf(procio, "/proc/%d/io", pid);
	int iofd = open(procio, O_RDONLY);
	if (iofd < 0){
		perror("open");
		exit(-1);
	}
	while( read(iofd, buffer, 4096) > 0){
		printf("%s\n", buffer);
	}
	return;
}

int main(int argc, char const *argv[]){
	/* Open data and Build structres */
	FILE* rqsout;
	char* line = NULL;
	size_t len = 0;
	ssize_t readcount;
	int i;
	long time, m_time;
	struct timespec* curtime = (struct timespec*)malloc(sizeof(struct timespec));

	rqsout = fopen("/home/iamos/Documents/inoti/rqsout", "r");
	if(rqsout == NULL){
		perror("fopen");
		exit(EXIT_FAILURE);
	}

	struct fileinfolist* listarray[NUM_OF_WATCH+1];
	for(i=0; i<NUM_OF_WATCH+1; i++){
		listarray[i] = NULL;
		// printf("list[%d] = (NULL)\n", i);
	}

	while( (readcount = getline(&line, &len, rqsout)) != -1){
		int index;
		char filepath[255];
		long offset;
		long length;
		sscanf(line,"%d,%[^,],%ld,%ld\n", &index, filepath, &offset, &length);
		if ( listarray[index] == NULL){
			printf("listarray[%d] is created\n", index);
			listarray[index] = new_fileinfolist();
		}
		struct fileinfo* filenode = new_fileinfo(filepath, offset, length);
		append(listarray[index], filenode);
	}
	printf("Build prefetching list ... Done.\n");

	FILE* triggerfile;
	triggerfile = fopen("/home/iamos/Documents/inoti/trgout","r");
	if(triggerfile == NULL){
		perror("fopen triggerfile ");
		exit(EXIT_FAILURE);
	}
	/* No error -> Make inotify instance (fd) */
	int notifd = inotify_init();
	if ( notifd < 0 ){
		perror("inotify_init");
		exit(EXIT_FAILURE);
	}
	/* attach wd to instance */
	int* wd = calloc(NUM_OF_WATCH, sizeof(int));
	if( wd == NULL){
		perror("calloc");
		exit(EXIT_FAILURE);
	}
	/* struct inotify_event */
	struct inotify_event* notivent = malloc(sizeof(struct inotify_event));
	if( notivent == NULL){
		perror("malloc");
		exit(EXIT_FAILURE);
	}
	int wdcount = 0;
	while( (readcount = getline(&line, &len, triggerfile)) != -1){
		int index;
		char filepath[255];
		sscanf(line,"%d,%[^\n]", &index, filepath);
		// printf("%s\n", filepath);
		wd[wdcount] = inotify_add_watch(notifd, filepath, IN_ACCESS | IN_ONESHOT);
		if( wd[wdcount] == -1){
			printf("  line 124: wd[%d] : %s\n", wdcount, filepath);
			switch (errno){
				case EACCES	:
				printf("Read access to the given file is not permitted.\n");
				break;
				case EBADF	:
				printf("The given inotify_instance is not valid.\n");
				break;
				case EFAULT :
				printf("pathname points outsize of the process's accessible address space.\n");
				break;
				case EINVAL :
				printf("The given event mask contains no valid events;or fd is not inotify instance.\n");
				break;
				case ENOENT :
				printf("A directory component in pathname does not exist or is a dangling symbolic link.\n");
				break;
				case ENOSPC :
				printf("The user limit on the total number of inotify watches was reached or the kernel failed to allocate a needed resource.\n");
				break;
				case ENOMEM :
				printf("Insufficient kernel memory was available.\n");
				break;
			} 
			exit(EXIT_FAILURE);
		}
		wdcount++;
	}

	print_procio();

	/* Check time */
	if( clock_gettime(CLOCK_MONOTONIC, curtime) < 0){
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}
	time = ((curtime->tv_sec * SEC_TO_NANO) + curtime->tv_nsec);
	
	/* Prefetch Before fork */
	struct fileinfo* initnode = listarray[0]->head;
	int count = 0;
	while( initnode != NULL){
		// printf("%s -> %p\n", initnode->filepath, initnode->next);
		int fd = open(initnode->filepath, O_RDONLY);
		off_t offset = initnode->offset;
		off_t length = initnode->length;
		while(1){
			// printf("%d ~ %d\n", offset, length);
			if( length > FADVISE_LIMIT){
				if( posix_fadvise(fd, offset, FADVISE_LIMIT, POSIX_FADV_WILLNEED) < 0 ){
					perror("posix_fadvise");
				}
				offset = offset + FADVISE_LIMIT;
				length = length - FADVISE_LIMIT;
			}
			else{
				if ( posix_fadvise(fd, offset, length, POSIX_FADV_WILLNEED) < 0){
					perror("posix_fadvise");
				}
				break;
			}
		}
		close(fd);
		initnode = initnode->next;
		count++;
	}
	/* Check time */
	if( clock_gettime(CLOCK_MONOTONIC, curtime) < 0){
		perror("clock_gettime");
		exit(EXIT_FAILURE);
	}
	m_time = ((curtime->tv_sec * SEC_TO_NANO) + curtime->tv_nsec);
	printf("@launch prefetch %d files.\n", count);
	printf("prefetching [ %f µs]\n", (m_time - time)*0.001);
	print_procio();

	pid_t pid = fork();
	/* Child */
	if (pid == 0){
		if (execl(argv[1], argv[1], NULL) < 0) {
			perror("execl");
			exit(-1);
		}
	}
	/* Parent */
	else{
		int ret;
		int status;
		while(1){

			/* Check time */
			// waitpid(-1, &status, WNOHANG);
			// if (WIFEXITED(status)){
			// 	printf("Child process is terminated.\n");
			// 	break;
			// }
			printf("Wating wd...\n");
			ret = read(notifd, notivent, sizeof(struct inotify_event));
			printf("@ wd[%d] Activating...\n", notivent->wd);
			if ( notivent->mask == 1){
				if( clock_gettime(CLOCK_MONOTONIC, curtime) < 0){
				perror("clock_gettime");
				exit(EXIT_FAILURE);
				}
				time = ((curtime->tv_sec * SEC_TO_NANO) + curtime->tv_nsec);
				/* Prefetching Here */
				/*pthread_t prefetch_thread;
				pthread_attr_t attr;
				if ( pthread_attr_init(&attr) < 0) {
					perror("pthread_attr_init");
					exit(EXIT_FAILURE);
				}
				if ( pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED) < 0) {
					perror("pthread_attr_setdetachstate");
					exit(EXIT_FAILURE);
				}
				if ( pthread_create(&prefetch_thread, &attr, do_prefetch, (void*)listarray[notivent->wd]) < 0) {
					perror("pthread_create");
					exit(EXIT_FAILURE);
				}*/

				struct fileinfo* initnode = listarray[notivent->wd]->head;
				int count = 0;
				while( initnode != NULL){
					// printf("%s -> %p\n", initnode->filepath, initnode->next);
					int fd = open(initnode->filepath, O_RDONLY);
					off_t offset = initnode->offset;
					off_t length = initnode->length;
					while(1){
						// printf("%d ~ %d\n", offset, length);
						if( length > FADVISE_LIMIT){
							if( posix_fadvise(fd, offset, FADVISE_LIMIT, POSIX_FADV_WILLNEED) < 0 ){
								perror("posix_fadvise");
							}
							offset = offset + FADVISE_LIMIT;
							length = length - FADVISE_LIMIT;
						}
						else{
							if ( posix_fadvise(fd, offset, length, POSIX_FADV_WILLNEED) < 0){
								perror("posix_fadvise");
							}
							break;
						}
					}
					close(fd);
					initnode = initnode->next;
					count++;
				}
							/* Check time */
				if( clock_gettime(CLOCK_MONOTONIC, curtime) < 0){
					perror("clock_gettime");
					exit(EXIT_FAILURE);
				}
				m_time = ((curtime->tv_sec * SEC_TO_NANO) + curtime->tv_nsec);
				printf("@launch prefetch %d files.\n", count);
				printf("prefetching [ %f µs]\n", (m_time - time)*0.001);
				print_procio();
			}
		}
	}
	return 0;
}