#define _GNU_SOURCE
#include <stdio.h>
#include <sys/inotify.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "tracer.h" 

#define NUM_OF_WATCH	4
#define BUFFER_SIZE 4096
#define SEC_TO_NANO 1000000000LL
#define NANO_TO_MIRCO 0.001
#define INTERVAL	1

int main(int argc, char const *argv[]){
	if(argc != 2){
		printf("./tracer [PATH]\n");
		exit(EXIT_FAILURE);
	}
	int cpid = fork();
	if(cpid < 0){
		perror("fork");
		exit(EXIT_FAILURE);
	}
	/* Child */
	if (cpid == 0){
		/* Wait until signals */
		signal(SIGUSR1, empty_handler);
		pause();
		#ifdef BIT32
		if( setenv("LD_PRELOAD", "/home/iamos/Documents/inoti/lib/collector32.so", 0) < 0){
			perror("setevn");
			exit(EXIT_FAILURE);
		}
		#else
		if( setenv("LD_PRELOAD", "/home/iamos/Documents/inoti/lib/collector.so", 0) < 0){
			perror("setevn");
			exit(EXIT_FAILURE);
		}
		#endif
		if( execl(argv[1], argv[1], NULL) < 0){
			perror("execl");
			exit(EXIT_FAILURE);
		}
		
	}
	/* Paretns*/
	else{
		printf("CPID = %d\n", cpid);

		/* child process data*/
		int status, ret_wait;

		/* Preparing open data */
		struct stat statinfo;
		char mapspath[255];
		char* buffer = (char*)malloc(4096);
		memset(buffer, '\0', BUFFER_SIZE);
		memset(mapspath, '\0', 255);
		sprintf(mapspath, "/proc/%d/maps", cpid);

		/* /proc/[pid]/maps data */
		unsigned long start, end;
		char flag_r,flag_w, flag_x, flag_s;
		unsigned long long pgoff;
		unsigned int major, minor;
		unsigned long ino = 0;
		char filepath[256];
		memset(filepath, '\0', 256);
		
		/* Data structure for VM_MAPPING */
		struct timespec* curtime = (struct timespec*)malloc(sizeof(struct timespec));
		long time, m_time;
		struct nodelist* nlist = newnodelist();

		/* /proc/diskstat data */
		char* diskstat = "/proc/diskstat";
		struct io_stats* iostats[2];
		struct io_stats* iobuffer;
		int io_major, io_minor;
		int cur = 0;
		char io_dev[256];
		char* io_stringbuffer = (char*)malloc(4096);
		if(io_stringbuffer == NULL){
			perror("malloc io_stringbuffer");
			exit(EXIT_FAILURE);
		}
		char* io_terms;
		memset(io_stringbuffer, '\0', 4096);

		iostats[0] = (struct io_stats*)malloc(sizeof(struct io_stats));
		iostats[1] = (struct io_stats*)malloc(sizeof(struct io_stats));
		iobuffer   = (struct io_stats*)malloc(sizeof(struct io_stats));

		/* Data structure for disk_stat node, list */
		struct stat_list* slist = new_stat_list();

		/* Initial io_stat data */
		int fd_diskstats;
		if( (fd_diskstats = open("/proc/diskstats", O_RDONLY)) < 0){
			perror("open /proc/diskstats");
			exit(-1);
		}	
		read(fd_diskstats, io_stringbuffer, 4096);
		io_terms = strtok(io_stringbuffer, "\n");
		while(io_terms != NULL){
			sscanf(io_terms, "%d %d %s %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu\n",
				&io_major, &io_minor, io_dev,
				&(iobuffer->readcomplete), &(iobuffer->readmerged), &(iobuffer->readsector), &(iobuffer->readspent),
				&(iobuffer->writecomplete), &(iobuffer->writemerged), &(iobuffer->writesector), &(iobuffer->writespent),
				&(iobuffer->IOpending), &(iobuffer->IOspent), &(iobuffer->IOwspent)
				);
			if (strncmp(io_dev, "sdb3", 4)== 0){
						/* Copy data from buffer */
				iostats[cur]-> readcomplete	= iobuffer->readcomplete;
				iostats[cur]-> readmerged	= iobuffer->readmerged;
				iostats[cur]-> readsector	= iobuffer->readsector;
				iostats[cur]-> readspent	= iobuffer->readspent;
				iostats[cur]-> writecomplete= iobuffer->writecomplete;
				iostats[cur]-> writemerged	= iobuffer->writemerged;
				iostats[cur]-> writesector	= iobuffer->writesector;
				iostats[cur]-> writespent	= iobuffer->writespent;
				iostats[cur]-> IOpending	= iobuffer->IOpending;
				iostats[cur]-> IOspent		= iobuffer->IOspent;
				iostats[cur]-> IOwspent		= iobuffer->IOwspent;
				cur = !cur;
				break;
			}
			io_terms = strtok(NULL, "\n");
		}

		/* wake up cild process */
		if (kill(cpid, SIGUSR1) < 0){
			perror("kill -> child");
			exit(EXIT_FAILURE);
		}

		while(1){
			printf("Wating..\n");
			/* Monitoring overhead */
			if( clock_gettime(CLOCK_MONOTONIC, curtime) < 0){
				perror("clock_gettime");
				exit(EXIT_FAILURE);
			}
			time = ((curtime->tv_sec * SEC_TO_NANO) + curtime->tv_nsec);

			/*  Monitoring Rutine 
				1. Check child prcess
				2. Open /proc/[pid]/maps
				3. Parse a maps string and build a structure */
			ret_wait = waitpid(cpid, &status, WNOHANG);

			/* if child still alive */
			if (ret_wait == 0){ 
				/* /proc/diskstat */
				if( (fd_diskstats = open("/proc/diskstats", O_RDONLY)) < 0){
					perror("open /proc/diskstats");
					exit(-1);
				}	
				read(fd_diskstats, io_stringbuffer, 4096);
				io_terms = strtok(io_stringbuffer, "\n");
				while(io_terms != NULL){
					sscanf(io_terms, "%d %d %s %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu\n",
						&io_major, &io_minor, io_dev,
						&(iobuffer->readcomplete), &(iobuffer->readmerged), &(iobuffer->readsector), &(iobuffer->readspent),
						&(iobuffer->writecomplete), &(iobuffer->writemerged), &(iobuffer->writesector), &(iobuffer->writespent),
						&(iobuffer->IOpending), &(iobuffer->IOspent), &(iobuffer->IOwspent)
						);
					if (strncmp(io_dev, "sdb3", 4)== 0){
						/* Copy data from buffer */
						iostats[cur]-> readcomplete	= iobuffer->readcomplete;
						iostats[cur]-> readmerged	= iobuffer->readmerged;
						iostats[cur]-> readsector	= iobuffer->readsector;
						iostats[cur]-> readspent	= iobuffer->readspent;
						iostats[cur]-> writecomplete= iobuffer->writecomplete;
						iostats[cur]-> writemerged	= iobuffer->writemerged;
						iostats[cur]-> writesector	= iobuffer->writesector;
						iostats[cur]-> writespent	= iobuffer->writespent;
						iostats[cur]-> IOpending	= iobuffer->IOpending;
						iostats[cur]-> IOspent		= iobuffer->IOspent;
						iostats[cur]-> IOwspent		= iobuffer->IOwspent;

						// printf("%f MB/s\t%lu\t%lu ms\n",
						// (iostats[cur]->readsector - iostats[!cur]->readsector) * 0.5 / 1024 ,
						// 	iostats[cur]->IOpending,
						// iostats[cur]->IOspent - iostats[!cur]->IOspent
						// );

						/* Store diskstat info */
						/* node = timestamp, redsector, iospent*/
						struct stat_node* temp_snode = new_stat_node(time,
							iostats[cur]->readsector - iostats[!cur]->readsector,
							iostats[cur]->IOspent - iostats[!cur]->IOspent);
						stat_list_append(slist, temp_snode);

						cur = !cur;
					}
					io_terms = strtok(NULL, "\n");
				}
				memset(io_stringbuffer, '\0', 4096);

				/* /proc/[pid]/maps */
				int print_on = 0;
				int fd_maps = open(mapspath, O_RDONLY);
				if( fd_maps < 0 ){
					perror("open");
					exit(EXIT_FAILURE);
				}
				while ( read(fd_maps, buffer, 4096) > 0){
					char* term = strtok(buffer, "\n");
					while ( term != NULL) {
						sscanf(term, "%lx-%lx %c%c%c%c %08llx %02x:%02x %lu %[^\n]\n", 
							&start, &end, &flag_r, &flag_w, &flag_x, &flag_s, &pgoff,
							&major, &minor, &ino, filepath );
						if (strncmp(filepath, "[stack]", 7) == 0){print_on = 0;}
						if ( print_on && (major == 8)){
							if( inlist(nlist, ino) == 0 ){
								struct node* new = newnode(filepath, time, ino);
								append(nlist, new);
							}
						}
						if (strncmp(filepath, "[heap]", 6) == 0){print_on = 1;}
						term = strtok(NULL, "\n");
					}
				}

			/* Monitoring overhead */
				if( clock_gettime(CLOCK_MONOTONIC, curtime) < 0){
					perror("clock_gettime");
					exit(EXIT_FAILURE);
				}
				m_time = ((curtime->tv_sec * SEC_TO_NANO) + curtime->tv_nsec);
				printf("monigoring %ld [ %f µs]\n", time, (m_time - time)*NANO_TO_MIRCO);
				printf("\n");
				sleep(INTERVAL);
			}
			/* Child die */
			else{
				/* Write mmap logs */
				int fd, logfd, statfd;
				struct node* temp = nlist->head;
				struct stat buf;
				if( (logfd = open("mmaplog", O_RDWR | O_CREAT | O_TRUNC, 00666)) < 0){
					perror("open logfd");
					exit(EXIT_FAILURE);
				}
				while(temp != NULL){
					fd = open(temp->filepath, O_RDONLY);
					if(fd < 0){
						perror("open nlist->node");
						printf("%s\n", temp->filepath);
					}
					else{
						fstat(fd, &buf);
						dprintf(logfd, "%s,%ld,%d,%zu\n", temp->filepath, temp->time, 0, buf.st_size);
					}
					temp = temp->next;
				}
				printf("Total files : %d\n", nlist->size);
				close(logfd);
				if ( (statfd = open("statlog", O_RDWR | O_CREAT | O_TRUNC, 00666)) < 0){
					perror("open statfd");
					exit(EXIT_FAILURE);
				}
				struct stat_node* s_temp = slist->head;
				while( s_temp != NULL){
					dprintf(statfd, "%ld,%lu,%lu\n", s_temp->timestamp, s_temp->readsector, s_temp->IOspent);
					s_temp = s_temp->next;
				}
				break;
			}
		}
	}
	return 0;
}