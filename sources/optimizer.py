# file - timestamp mapping
import sys
import operator
import math

MEGA_TO_BYTES = 1048576
def get_closest(access_time, now):
	gap = float('inf')
	filepath = ""
	for x in access_time:
		atime =  (access_time[x] - inittime)*1.0 / 1000000000
		if ( abs(gap) > abs(atime - now)):
			gap = abs(atime - now)
			# print gap, atime
			filepath = x
	return filepath

def mergedataset (dataset):
	# dataset = [path, offset, length, lbn]
	print "@MergeDataSet"
	newdataset = []
	filedict = {}
	for lx in list(dataset):
		filename = lx[0]
		# fileinfo = [int(lx[1]), int(lx[2]), int(lx[3])]
		fileinfo = [int(lx[1]), int(lx[2])]
		if filename in filedict:
			if fileinfo not in filedict[filename]:
				filedict[filename].append(fileinfo)
		else:
			filedict[filename] = [fileinfo]

	for x in filedict:
		filelists = sorted(list(filedict[x]))
		sortedlist = []
		while True:
			if len(filelists) == 0:
				break
			elif len(filelists) == 1:
				thisblock = filelists.pop(0)
				sortedlist.append(thisblock)
			else:
				filelists.sort(key= lambda lam : lam[0])
				left = filelists.pop(0)
				right = filelists.pop(0)

				if left[0]+left[1] < right[0]:
					sortedlist.append(left)
					filelists.insert(0, right)
					continue
				elif left[0]+left[1] == right[0]:
					left[1] = right[0]+right[1]-left[0]
					filelists.insert(0, left)
					continue
				elif left[0]+left[1] == right[0]+right[1]:
					if left[0] <= right[0]:
						# Discard right
						filelists.insert(0, left)
						continue
					elif left[0] > right[0]:
						# Discard left
						filelists.insert(0, right)
						continue
				elif left[0] <= right[0]:
					if left[0]+left[1] < right[0]+right[1]:
						left[1] = right[0]+right[1]-left[0]
						filelists.insert(0,left)
						continue
					elif left[0]+left[1] >= right[0]+right[1]:
						filelists.insert(0, left)
						continue
				
				print "left  : {:10} ~ {:10}".format(left[0], left[0]+left[1])
				print "right : {:10} ~ {:10}".format(right[0], right[0]+right[1])
				print ""

		for offsets in sortedlist:
			# temp = [x, offsets[0], offsets[1], offsets[2]]
			temp = [x, offsets[0], offsets[1]]
			newdataset.append(temp)
	return newdataset

def my_kde(statdict, width):
	tempdict = {}
	for x in xrange(0,len(statdict)):
		tempdict[x] = 0
	dictsize = len(statdict)-1
	if width == 3:
		epan = [0.5625, 0.75, 0.5625]
		for key in tempdict:
			# indices = [key-1, key, key+1]
			if 0 < key < dictsize :
				tempdict[key-1] += statdict[key]*epan[0]
				tempdict[key]   += statdict[key]*epan[1]
				tempdict[key+1] += statdict[key]*epan[2]
		
			elif key == 0:
				tempdict[key]   += statdict[key]*epan[1]
				tempdict[key+1] += statdict[key]*epan[2]
		
			elif key == dictsize:
				tempdict[key-1] += statdict[key]*epan[0]
				tempdict[key]   += statdict[key]*epan[1]
			else:
				print "Error"		
		for x in tempdict:
			tempdict[x] = tempdict[x]/1.87

	elif width == 5:
		epan = [0.4167, 0.6667, 0.75, 0.6667, 0.4167]
		for key in tempdict:
			if 1 < key < dictsize - 1:
				tempdict[key-2] += statdict[key]*epan[0]
				tempdict[key-1] += statdict[key]*epan[1]
				tempdict[key]   += statdict[key]*epan[2]
				tempdict[key+1] += statdict[key]*epan[3]
				tempdict[key+2] += statdict[key]*epan[4]
			elif key == 0:
				tempdict[key]   += statdict[key]*epan[2]
				tempdict[key+1] += statdict[key]*epan[3]
				tempdict[key+2] += statdict[key]*epan[4]
			elif key == 1:
				tempdict[key-1] += statdict[key]*epan[1]
				tempdict[key]   += statdict[key]*epan[2]
				tempdict[key+1] += statdict[key]*epan[3]
				tempdict[key+2] += statdict[key]*epan[4]
			elif key == dictsize:
				tempdict[key-2] += statdict[key]*epan[0]
				tempdict[key-1] += statdict[key]*epan[1]
				tempdict[key]   += statdict[key]*epan[2]
			elif key == dictsize-1:
				tempdict[key-2] += statdict[key]*epan[0]
				tempdict[key-1] += statdict[key]*epan[1]
				tempdict[key]   += statdict[key]*epan[2]
				tempdict[key+1] += statdict[key]*epan[3]
			else:
				print "Error"
		for x in tempdict:
			tempdict[x] = tempdict[x]/2.9168
			print "{}, {}".format(x, tempdict[x])
	return tempdict

class Node(object):
	"""docstring for Node"""
	def __init__(self, index, spent):
		super(Node, self).__init__()
		self.index = index
		self.spent = spent
		self.next  = None

class linkedlist(object):
	"""docstring for linkedlist"""
	def __init__(self):
		super(linkedlist, self).__init__()
		self.head = None
		self.tail = None
	
	def append(self, node):
		if self.head == None:
			self.head = node
			self.tail = node
		else:
			self.tail.next = node
			self.tail = node

	def show(self):
		temp = self.head
		while temp is not None:
			print "[{}]\t{:>8.4f}".format(temp.index, temp.spent)
			temp = temp.next

class AccessInfo(object):
	"""docstring for AccessInfo"""
	def __init__(self, filepath, offset, length, time):
		super(AccessInfo, self).__init__()
		self.filepath = filepath
		self.time = int(time)
		self.offset = int(offset)
		self.length = int(length)
		self.next = None
		self.prev = None
		
class AccessInfoList(object):
	"""docstring for AccessInfoList"""
	def __init__(self):
		super(AccessInfoList, self).__init__()
		self.size = 0
		self.head = None
		self.tail = None
		self.inittime = None

	def append(self, node):
		if self.head == None:
			self.head = node
			self.tail = node
			self.size +=1
			self.inittime = node.time
		else:
			node.prev = self.tail
			self.tail.next = node
			self.tail = node
			self.size +=1
			if node.time < self.inittime:
				self.inittime = node.time

	def delfilepath(self, pname):
		if self.head.filepath == pname:
			self.head = self.head.next
		count = 0
		cur = self.head
		while cur is not None:
			if cur.filepath == pname:
				cur.prev.next = cur.next
				cur.next.prev = cur.prev
				count += 1
				self.size +=-1
			cur = cur.next
		print "Del {} nodes".format(count)

	def Hasfname(self, pname):
		cur = self.head
		count = 0
		while cur is not None:
			if cur.filepath == pname:
				count += 1
			cur = cur.next
		if count > 0:
			# print "{} , {} times".format(pname, count)
			return True
		else:
			# print "False"
			return False

	def popByfilepath(self, pname):
		# ret = AccessInfoList()
		ret = []
		# ret.inittime = self.inittime
		# return AccessInfoList with pname
		cur = self.head
		while cur is not None:
			if cur.filepath == pname:
				if cur == self.head :
					self.head = self.head.next
					if self.head is not None:
						self.head.prev = None
				elif cur == self.tail :
					self.tail = self.tail.prev
					self.tail.next = None
				else:
					cur.prev.next = cur.next
					cur.next.prev = cur.prev
				self.size += -1
				ret.append([cur.filepath, cur.offset, cur.length])
			cur = cur.next
		return ret

	def pophead(self):
		""" Return head node 
			1. Copy node
			2. rearrange InfoList	
		"""
		retnode = AccessInfo(filepath= self.head.filepath, 
								   time  = self.head.time,
								  offset = self.head.offset,
								  length = self.head.length)
		if self.head.next is not None:
			self.head = self.head.next
			self.head.prev = None
			self.size += -1
		else:
			self.head = None
			self.tail = None
			self.size = 0
		return retnode

	def getFileListBetween(self, start, end):
		minval = int(start)
		maxval = int(end)
		retarray = AccessInfoList()
		cur = self.head
		# print inittime
		# print (cur.time - inittime ) // 1000000000
		while cur is not None:
			curindex = int((cur.time - self.inittime) // 1000000000)
			if minval <= curindex <= maxval:
				tempnode = AccessInfo(filepath=cur.filepath, time=cur.time, offset=cur.offset, length=cur.length)
				retarray.append(tempnode)
			cur = cur.next
		return retarray

	def show(self):
		temp = self.head
		while temp is not None:
			curtime = int((temp.time - self.inittime) // 1000000000)
			print "{}, {}, {} ~ {}".format(curtime, temp.filepath, temp.offset, temp.offset+temp.length)
			temp = temp.next

	def merge(self):
		# pop same file and merge it
		print "Orig size = {}".format(self.size)
		retarray = AccessInfoList()
		retarray.inittime = self.inittime
		while self.head is not None:
			pivot = self.pophead()
			# print self.size
			# find pivot
			if self.Hasfname(pivot.filepath) == False:
				retarray.append(pivot)

			else: # POP all same files
				temparr = []
				temparr.append([pivot.filepath, pivot.offset, pivot.length])
				while self.Hasfname(pivot.filepath) == True:
					pathlist = self.popByfilepath(pivot.filepath)
					for x in pathlist:
						temparr.append(x)
				temparr = sorted(temparr)
				
				# if len(temparr) > 1000:
				# 	print "{} has {} chunks".format(pivot.filepath, len(temparr))
				sortedlist = []
				left = temparr.pop(0)
				right = temparr.pop(0)
				while len(temparr) > 0:
					if left[1]+left[2] < right[1]:
						sortedlist.append(left)
						left = right
						right = temparr.pop(0)
						continue
					elif left[1]+left[2] == right[1]:
						left[2] = right[1]+right[2]-left[1]
						right = temparr.pop(0)
						continue
					elif left[1]+left[2] == right[1]+right[2]:
						if left[1] <= right[1]:
							# Discard right
							# temparr.insert(0, left)
							right = temparr.pop(0)
							continue
						elif left[1] > right[1]:
							# Discard left
							left = right
							right = temparr.pop(0)
							continue
					elif left[1] <= right[1]:
						if left[1]+left[2] < right[1]+right[2]:
							left[2] = right[1]+right[2]-left[1]
							right = temparr.pop(0)
							continue
						elif left[1]+left[2] >= right[1]+right[2]:
							right = temparr.pop(0)
							continue
				# remainder cleanup
				if left[1]+left[2] < right[1]:
					sortedlist.append(left)
					sortedlist.append(right)
				elif left[1]+left[2] == right[1]:
					left[2] = right[1]+right[2]-left[1]
					sortedlist.append(left)
				elif left[1]+left[2] == right[1]+right[2]:
					if left[1] <= right[1]:
						sortedlist.append(left)
					elif left[1] > right[1]:
						sortedlist.append(right)
				elif left[1] <= right[1]:
					if left[1]+left[2] < right[1]+right[2]:
						left[2] = right[1]+right[2]-left[1]
						sortedlist.append(left)
					elif left[1]+left[2] >= right[1]+right[2]:
						sortedlist.append(left)

				if len(temparr) > 0:
					print "Error"
				
				for x in sortedlist:
					newa = AccessInfo(filepath=x[0], time=pivot.time, offset=x[1], length=x[2])
					retarray.append(newa)
					# print x
		print "retarray.size = {}".format(retarray.size)
		# retarray.show()
		return retarray

	def toarray(self):
		ret = []
		cur = self.head
		while cur is not None:
			inline = [cur.filepath, cur.offset, cur.length]
			ret.append(inline)
			cur = cur.next
		return ret

# inputfile = "firefox"
inputfile = sys.argv[1]
readfile = open("/home/iamos/Documents/inoti/log/readlog_{}".format(inputfile), 'r')
mmapfile = open("/home/iamos/Documents/inoti/log/mmaplog_{}".format(inputfile), 'r')
statfile = open("/home/iamos/Documents/inoti/log/statlog_{}".format(inputfile), 'r')

readrow = []
mmaprow = []
statrow = []
farray = AccessInfoList()
# farray = []

first_aceess_time = {}

while True:
	line = readfile.readline()
	if not line : break
	terms = line.rstrip('\n').split(',')
	readrow.append(terms)
	if terms[0] in first_aceess_time:
		# print int(first_aceess_time[terms[0]]) , int(terms[1])
		if int(terms[1]) < first_aceess_time[terms[0]]:
			print renew
			first_aceess_time[terms[0]] = int(terms[1]) 
	else:
		first_aceess_time[terms[0]] = int(terms[1])
	farray.append( AccessInfo(filepath=terms[0], time=terms[1], offset=terms[2], length=terms[3]) )


while True:
	line = mmapfile.readline()
	if not line : break
	terms = line.rstrip('\n').split(',')
	mmaprow.append(terms)
	farray.append( AccessInfo(filepath=terms[0], time=terms[1], offset=terms[2], length=terms[3]) )

while True:
	line = statfile.readline()
	if not line : break
	terms = line.rstrip('\n').split(',')
	statrow.append(terms)

readfile.close()
mmapfile.close()
statfile.close()

read_inittime = 9223372036854775807
mmap_inittime = 9223372036854775807
stat_inittime = 9223372036854775807

if len(readrow) > 0:
	read_inittime = int(readrow[0][1])
if len(mmaprow) > 0:
	mmap_inittime = int(mmaprow[0][1])
if len(statrow) > 0:
	stat_inittime = int(statrow[0][0])

inittime = min(read_inittime, mmap_inittime)
inittime = min(inittime, stat_inittime)
if inittime < farray.inittime :
	farray.inittime = inittime

timeslice = 1000000000 # 1 Second
MStoSEC	  = 1000 # 1000 Milliseconds = 1 second

block_dist = {}
totalblock = 0

# Grasp read log
for x in readrow:
	index = (int(x[1])-inittime) // timeslice
	thisblock = int(x[3])
	totalblock += thisblock

	if index in block_dist:
		block_dist[index]+=thisblock
	else:
		block_dist[index] = thisblock
# Grasping mmap log
for x in mmaprow:
	index = (int(x[1])-inittime) // timeslice
	thisblock = int(x[3])
	totalblock += thisblock

	if index in block_dist:
		block_dist[index]+=thisblock
	else:
		block_dist[index] = thisblock

average = totalblock/len(block_dist)

# OPT
NUMOFPOINT   = 2
OPTthreshold = 100
minval = []
for x in statrow:
	cur_slice = (int(x[0])-inittime) // timeslice
	cur_iospent = int(x[2])

# Print Current block distribution
statdict = {}
# print "{}\t{}\t{}".format("Timeslice", "r_Bytes", "IOspent")
for x in statrow:
	time = (int(x[0])-inittime)//timeslice
	spent = int(x[2])
	statdict[time] = spent
	# print "{:>5}\t{:>12}\t{:>6}".format( (int(x[0])-inittime)//timeslice , x[1], x[2] )

statdict = my_kde(statdict, 3)

print "After KDE(width=3)"
print "{}\t{}".format("Time slice", "IOspent")
for x in statdict:
	print "{:>5}\t\t{:>8.4f}".format(x, statdict[x])


listhead = linkedlist()
for x in statdict:
	listhead.append(Node(int(x), float(statdict[x])))

# # listhead.show()

allslice = []
temp = listhead.head
while temp is not None:
	allslice.append(int(temp.index))
	temp = temp.next

triggermap = {}


# targettime = 43 # Civ5
# targettime = 16 # Poe
# slice1 = allslice[:targettime]
# slice2 = allslice[targettime:]

# temppname = get_closest(first_aceess_time, targettime)
# print temppname

# farray.Hasfname(temppname)
# 1. Del trigger file in farray
# farray.delfilepath(temppname)
print "farray.size = {}".format(farray.size)

# # 2. Find slice[A:B] in faaray, get filelist
# a = farray.getFileListBetween(min(slice1), max(slice1))
# aa = a.merge()
# b = farray.getFileListBetween(min(slice2), max(slice2))
# bb = b.merge()
# merged = [aa, bb]
# triggermap[1] = get_closest(first_aceess_time, targettime)

aa = farray.merge()
merged = [aa]

counter = 0
rqsout = open("/home/iamos/Documents/inoti/rqsout","w")
for big_chunk in merged:
	cur = big_chunk.head
	while cur is not None:
		line = "{},{},{},{}\n".format(counter, cur.filepath, cur.offset, cur.length)
		# print line
		rqsout.writelines(line)
		cur = cur.next
	counter += 1
rqsout.close()

trgout = open("/home/iamos/Documents/inoti/trgout","w")
for x in triggermap:
	line = "{},{}\n".format(x, triggermap[x])
	trgout.writelines(line)
trgout.writelines("")
trgout.close()
