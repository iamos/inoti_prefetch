// #include "wrapper_header.h"
#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <time.h>

#define SEC_TO_NANO 1e9

int glob_fd;
struct timespec* curtime;

int __libc_start_main(int (*main) (int, char **, char **), int argc, char **ubp_av,
                      void (*init) (void), void (*fini)(void), void (*rtld_fini)(void), void (*stack_end)) {
	int (*original__libc_start_main)(int (*main) (int, char **, char **), int argc, char **ubp_av,
	                                 void (*init) (void), void (*fini)(void), void (*rtld_fini)(void), void (*stack_end));
	
	// printf("App start\n");

	glob_fd = open("readlog", O_RDWR | O_APPEND | O_CREAT , 00666);
	if (glob_fd < 0){
		perror("open");
		exit(EXIT_FAILURE);
	}

	curtime = (struct timespec*)malloc(sizeof(struct timespec));
	original__libc_start_main = dlsym(RTLD_NEXT, "__libc_start_main");
	return original__libc_start_main(main, argc, ubp_av, init, fini, rtld_fini, stack_end);
}

ssize_t read(int fd, void *buf, size_t count) {
	ssize_t (*original_read)(int fd, void *buf, size_t count);
	ssize_t ret;
	original_read = dlsym(RTLD_NEXT, "read");
	ret = (*original_read)(fd, buf, count);
	if (glob_fd > 0){
		if( clock_gettime(CLOCK_MONOTONIC, curtime) < 0){
			perror("clock_gettime");
			exit(EXIT_FAILURE);
		}
		#ifdef BIT32
		long long cur_time = ((curtime->tv_sec * SEC_TO_NANO) + curtime->tv_nsec);
		#else
		long cur_time = ((curtime->tv_sec * SEC_TO_NANO) + curtime->tv_nsec);
		#endif
		size_t f_offset = lseek(fd, 0, SEEK_CUR);
		char procpath[255];
		char fpath[255];
		memset(procpath, '\0', 255);
		memset(fpath, '\0', 255);
		sprintf(procpath, "/proc/self/fd/%d", fd);
		if (readlink(procpath, fpath, 255) < 0) {
			perror("readlink");
			exit(EXIT_FAILURE);
		}
		if (!( strncmp(fpath, "anon_inode:", 11) == 0
			|| strncmp(fpath, "socket:", 7)	== 0
			|| strncmp(fpath, "pipe:", 5)	== 0
			|| strncmp(fpath, "/sys", 4) 	== 0
			|| strncmp(fpath, "/run", 4)	== 0
			|| strncmp(fpath, "/proc", 5)	== 0
			) ){
		#ifdef BIT32
			dprintf(glob_fd, "%s,%lld,%zu,%zu\n", fpath, cur_time, f_offset, ret);
		#else
			dprintf(glob_fd, "%s,%ld,%zu,%zu\n", fpath, cur_time, f_offset, ret);
		#endif
		}
	}
	return ret;
}

size_t fread(void *ptr, size_t size, size_t nmemb, FILE *stream){
	size_t (*original_fread)(void *ptr, size_t size, size_t nmemb, FILE *stream);
	size_t ret;
	original_fread = dlsym(RTLD_NEXT, "fread");	
	ret = (*original_fread)(ptr, size, nmemb, stream);

	if( glob_fd > 0){
		if( clock_gettime(CLOCK_MONOTONIC, curtime) < 0){
			perror("clock_gettime");
			exit(EXIT_FAILURE);
		}
		char procpath[255];
		char fpath[255];

		#ifdef BIT32
		long long cur_time = ((curtime->tv_sec * SEC_TO_NANO) + curtime->tv_nsec);
		#else
		long cur_time = ((curtime->tv_sec * SEC_TO_NANO) + curtime->tv_nsec);
		#endif
		int fd = fileno(stream);
		size_t f_offset = ftell(stream);
		memset(procpath, '\0', 255);
		memset(fpath, '\0', 255);
		sprintf(procpath, "/proc/self/fd/%d", fd);
		if (readlink(procpath, fpath, 255) < 0) {
			perror("readlink");
			exit(EXIT_FAILURE);
		}
		if (!( strncmp(fpath, "anon_inode:", 11) == 0
			|| strncmp(fpath, "socket:", 7)	== 0
			|| strncmp(fpath, "pipe:", 5)	== 0
			|| strncmp(fpath, "/sys", 4) 	== 0
			|| strncmp(fpath, "/run", 4)	== 0
			|| strncmp(fpath, "/proc", 5)	== 0
			) ){
			#ifdef BIT32
			dprintf(glob_fd, "%s,%lld,%zu,%zu\n", fpath, cur_time, f_offset, size * ret);
			#else
			dprintf(glob_fd, "%s,%ld,%zu,%zu\n", fpath, cur_time, f_offset, size * ret);

			#endif
		}

	}
	return ret;
}