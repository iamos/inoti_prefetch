/* tracer.h*/
// #include <time.h>

struct node{
	char filepath[512];
	long time;
	unsigned long ino;
	struct node* next;
};

struct nodelist{
	struct node* head;
	struct node* tail;
	int size;
};

void append(struct nodelist* nlist, struct node* node){
	if ( nlist->head != NULL){
		nlist->tail->next = node;
		nlist->tail = node;
		nlist->size++;
	}
	else{
		nlist->head = node;
		nlist->tail = node;
		nlist->size++;
	}

}

int inlist(struct nodelist* nlist, int ino){
	/* Return 1 if ino is in the list. 
	   Return 0 if ino is not in the list*/
	struct node* temp = nlist->head;
	while(temp != NULL){
		// printf("temp->ino = %ld\n", temp->ino);
		if(temp->ino == ino){
			return 1;
		}
		temp = temp->next;
	}
	return 0;
}

void printlist(struct nodelist* nlist){
	struct node* temp = nlist->head;
	int fd;
	struct stat buf;
	while(temp != NULL){
		// printf("%ld, %s\n", temp->time, temp->filepath);
		fd = open(temp->filepath, O_RDONLY);
		if(fd<0)exit(EXIT_FAILURE);
		fstat(fd, &buf);
		printf("%ld, %-80s (%ld)\n", temp->time, temp->filepath, buf.st_size);
		temp = temp->next;
	}
	printf("Count = %d\n", nlist->size );
}

struct node* newnode(char* filepath, long time, unsigned long ino){
	struct node* new = (struct node*)malloc(sizeof(struct node));
	if (new == NULL){
		perror("malloc node");
		exit(EXIT_FAILURE);
	}
	memset(new->filepath, '\0', 512);
	int i = 0;
	while(filepath[i] != '\0'){
		new->filepath[i] = filepath[i];
		i = i+1;
	}
	new->ino = ino;
	new->time = time;
	new->next = NULL;
	return new;
}

struct nodelist* newnodelist(){
	struct nodelist* new = (struct nodelist*)malloc(sizeof(struct nodelist));
	if(new == NULL){
		perror("malloc nodelist");
		exit(EXIT_FAILURE);
	}
	new->head = NULL;
	new->tail = NULL;
	new->size = 0;
	return new;
}

/* /proc/diskstat */
struct io_stats {
	unsigned long readcomplete;
	unsigned long readmerged;
	unsigned long readsector;
	unsigned long readspent;

	unsigned long writecomplete;
	unsigned long writemerged;
	unsigned long writesector;
	unsigned long writespent;

	unsigned long IOpending;
	unsigned long IOspent;
	unsigned long IOwspent;
};

struct stat_node{
	long timestamp;
	unsigned long readsector;
	unsigned long IOspent;
	struct stat_node* next;
};

struct stat_list{
	struct stat_node* head;
	struct stat_node* tail;
	int size;
};

struct stat_node* new_stat_node(long timestamp, unsigned long readsector, unsigned long IOspent){
	struct stat_node* new_snode = (struct stat_node*)malloc(sizeof(struct stat_node));
	if( new_snode == NULL){
		perror("malloc stat_node");
		exit(EXIT_FAILURE);
	}
	new_snode->timestamp = timestamp;
	new_snode->readsector = readsector;
	new_snode->IOspent = IOspent;
	new_snode->next = NULL;
	return new_snode;
}

struct stat_list* new_stat_list(){
	struct stat_list* newn = (struct stat_list*)malloc(sizeof(struct stat_list));
	if( newn == NULL){
		perror("malloc stat_list");
		exit(EXIT_FAILURE);
	}
	newn->head = NULL;
	newn->tail = NULL;
	newn->size = 0;
	return newn;
}

void stat_list_append(struct stat_list* list,struct stat_node* node){
	if(list->size > 0){
		list->tail->next = node;
		list->tail = node;
		list->size+=1;
		return;
	}
	else if (list->size == 0){
		list->head = node;
		list->tail = node;
		list->size += 1;
		return;
	}
	else{
		printf("Error : stat_list_append, invalid status\n");
		exit(EXIT_FAILURE);
		return;
	}
}

void empty_handler(int signo){
	return;
}