# from sklearn.neighbors.kde import KernelDensity
# import numpy as np

def _epan(u):
	return 3.0/4*(1-u**2)

statfile = open("/home/iamos/Documents/inoti/log/statlog_poe", 'r')
statrow = []
stat_val = {}

while True:
	line = statfile.readline()
	if not line : break
	terms = line.rstrip('\n').split(',')
	statrow.append(terms)

statfile.close()

inittime = int(statrow[0][0])
for row in statrow:
	x = (int(row[0])-inittime) // 1000000000
	y = int(row[2])
	stat_val[x] = y

# epan3 = [0.5625, 0.75, 0.5625]
# epan5 = [0.4167, 0.6667, 0.75, 0.6667, 0.4167]

def my_kde(statdict, width):
	tempdict = {}
	for x in xrange(0,len(statdict)):
		tempdict[x] = 0
	dictsize = len(statdict)-1
	if width == 3:
		epan = [0.5625, 0.75, 0.5625]
		for key in tempdict:
			# indices = [key-1, key, key+1]
			if 0 < key < dictsize :
				tempdict[key-1] += statdict[key]*epan[0]
				tempdict[key]   += statdict[key]*epan[1]
				tempdict[key+1] += statdict[key]*epan[2]
		
			elif key == 0:
				tempdict[key]   += statdict[key]*epan[1]
				tempdict[key+1] += statdict[key]*epan[2]
		
			elif key == dictsize:
				tempdict[key-1] += statdict[key]*epan[0]
				tempdict[key]   += statdict[key]*epan[1]
			else:
				print "Error"		
		for x in tempdict:
			tempdict[x] = tempdict[x]/1.87

	elif width == 5:
		epan = [0.4167, 0.6667, 0.75, 0.6667, 0.4167]
		for key in tempdict:
			if 1 < key < dictsize - 1:
				tempdict[key-2] += statdict[key]*epan[0]
				tempdict[key-1] += statdict[key]*epan[1]
				tempdict[key]   += statdict[key]*epan[2]
				tempdict[key+1] += statdict[key]*epan[3]
				tempdict[key+2] += statdict[key]*epan[4]
			elif key == 0:
				tempdict[key]   += statdict[key]*epan[2]
				tempdict[key+1] += statdict[key]*epan[3]
				tempdict[key+2] += statdict[key]*epan[4]
			elif key == 1:
				tempdict[key-1] += statdict[key]*epan[1]
				tempdict[key]   += statdict[key]*epan[2]
				tempdict[key+1] += statdict[key]*epan[3]
				tempdict[key+2] += statdict[key]*epan[4]
			elif key == dictsize:
				tempdict[key-2] += statdict[key]*epan[0]
				tempdict[key-1] += statdict[key]*epan[1]
				tempdict[key]   += statdict[key]*epan[2]
			elif key == dictsize-1:
				tempdict[key-2] += statdict[key]*epan[0]
				tempdict[key-1] += statdict[key]*epan[1]
				tempdict[key]   += statdict[key]*epan[2]
				tempdict[key+1] += statdict[key]*epan[3]
			else:
				print "Error"
		for x in tempdict:
			tempdict[x] = tempdict[x]/2.9168
			print "{}, {}".format(x, tempdict[x])
	return tempdict

# print stat_val
my_kde(stat_val, 3)