/* prefetch.h */
/* Define structures and API functions */

struct fileinfo {
	char* filepath;
	int offset;
	int length;
	struct fileinfo* next;
};

struct fileinfolist{
	struct fileinfo* head;
	struct fileinfo* tail;
	int size;
};

struct fileinfo* new_fileinfo(char* filepath, int offset, int length){
	struct fileinfo* newnode = (struct fileinfo*)malloc(sizeof(struct fileinfo));
	newnode->filepath = malloc(strlen(filepath) + 1);
	strncpy(newnode->filepath, filepath, strlen(filepath) + 1);
	newnode->offset = offset;
	newnode->length = length;
	newnode->next = NULL;
	return newnode;
}

struct fileinfolist* new_fileinfolist(){
	struct fileinfolist* newlist = (struct fileinfolist*)malloc(sizeof(struct fileinfolist));
	newlist->head = NULL;
	newlist->tail = NULL;
	newlist->size = 0;
	return newlist;
}

void append(struct fileinfolist* list, struct fileinfo* node){
	if (list->size == 0){
		list->head = node;
		list->tail = node;
	}
	else{
		list->tail->next = node;
		list->tail = node;
	}
	list->size += 1;
}