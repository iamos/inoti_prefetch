/* parse.c */
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
// #include <stdarg.h>

#define BUFFER_SIZE 4096
int main(int argc, char const *argv[]){
	/* code */
	// int fd = open("/proc/self/maps", O_RDONLY);
	int fd = open("./dump", O_RDONLY);
	if (fd < 0 ){
		perror("open");
		exit(EXIT_FAILURE);
	}
	char* buffer = (char*)malloc(4096);
	memset(buffer, '\0', BUFFER_SIZE);
	int len = read(fd, buffer, BUFFER_SIZE);
	printf("%d\n", len);

	char* term = strtok(buffer, "\n");

	/* /proc/[pid]/maps data */
	unsigned long start, end;
	char flag_r,flag_w, flag_x, flag_s;
	unsigned long long pgoff;
	unsigned int major, minor;
	unsigned long ino = 0;
	char filepath[256];

	int print_on = 0;
	while( term != NULL) {
		// printf("%s\n", term);
		sscanf(term, "%lx-%lx %c%c%c%c %08llx %02x:%02x %lu %s\n", 
			&start, &end, 
			&flag_r, &flag_w, &flag_x, &flag_s,
			&pgoff,
			&major, &minor,
			&ino,
			filepath );

		if (strncmp(filepath, "[stack]", 7) == 0){
			print_on = 0;
		}
		if( print_on ){
			printf("%lx - %lx (%ld, %ld pages), %s\n", start, end, end-start, (end-start)/4096 ,filepath);
			memset(filepath, '\0', 256);
		}
		if (strncmp(filepath, "[heap]", 6) == 0){
			print_on = 1;
		}

		term = strtok(NULL, "\n");
	}
	free(buffer);
	return 0;
}