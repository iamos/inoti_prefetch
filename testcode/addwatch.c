/* addwatch.c */
#include <stdio.h>
#include <sys/inotify.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char const *argv[]){
	char a[] = "/home/iamos/.config/unity3d/Obsidian Entertainment/Pillars of Eternity/NewsArchive.xml\n";
	printf("%s\n", a);
	int notifd = inotify_init();
	if ( inotify_add_watch(notifd, a, IN_ACCESS | IN_ONESHOT) < 0){
		perror("inotify_add_watch");
		exit(-1);
	}
	read(notifd, NULL, 0);
	/* code */
	return 0;
}