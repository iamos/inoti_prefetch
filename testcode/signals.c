#define _GNU_SOURCE
#include <stdio.h>
#include <sys/inotify.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

void sig_hanndler(int signo){
	return;
}
int main(int argc, char const *argv[]){
	int pid = fork();
	if( pid < 0){
		perror("fork");
		exit(-1);
	}
	if( pid == 0){
		printf("Child.\n");
		signal(SIGUSR1, sig_hanndler);
		pause();
		printf("Catcha\n");
	}
	else{
		printf("Parents do this.\n");
		sleep(3);
		printf("Send signal\n");
		kill(pid, SIGUSR1);
	}
	return 0;
}