#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

struct io_stats {
	unsigned long readcomplete;
	unsigned long readmerged;
	unsigned long readsector;
	unsigned long readspent;

	unsigned long writecomplete;
	unsigned long writemerged;
	unsigned long writesector;
	unsigned long writespent;

	unsigned long IOpending;
	unsigned long IOspent;
	unsigned long IOwspent;
};

int main(int argc, char const *argv[]){
	char buffer[4096];
	char* terms;
	int fd, cur = 0;
	memset(buffer, '\0', 4096);
	/* Data diskstat*/
	struct io_stats* iostats[2];
	struct io_stats* iobuffer;
	iostats[0] = (struct io_stats*)malloc(sizeof(struct io_stats));
	iostats[1] = (struct io_stats*)malloc(sizeof(struct io_stats));
	iobuffer = (struct io_stats*)malloc(sizeof(struct io_stats));

	int major, minor;
	char dev[256];
	int init = 0;
	while(1){
		if( (fd = open("/proc/diskstats", O_RDONLY)) < 0){
			perror("open /proc/diskstats");
			exit(-1);
		}
		read(fd, buffer, 4096);
		// printf("%s\n", buffer);
		printf("\n");
		terms = strtok(buffer, "\n");
		while( terms != NULL){
			// printf("%s\n", terms);
			sscanf(terms, "%d %d %s %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu %lu\n",
				&major, &minor, dev,
				&(iobuffer->readcomplete), &(iobuffer->readmerged), &(iobuffer->readsector), &(iobuffer->readspent),
				&(iobuffer->writecomplete), &(iobuffer->writemerged), &(iobuffer->writesector), &(iobuffer->writespent),
				&(iobuffer->IOpending), &(iobuffer->IOspent), &(iobuffer->IOwspent)
				);
			if (strncmp(dev, "sdb3", 4)== 0){
				/* Copy data from buffer */
				iostats[cur]-> readcomplete	= iobuffer->readcomplete;
				iostats[cur]-> readmerged	= iobuffer->readmerged;
				iostats[cur]-> readsector	= iobuffer->readsector;
				iostats[cur]-> readspent	= iobuffer->readspent;
				iostats[cur]-> writecomplete= iobuffer->writecomplete;
				iostats[cur]-> writemerged	= iobuffer->writemerged;
				iostats[cur]-> writesector	= iobuffer->writesector;
				iostats[cur]-> writespent	= iobuffer->writespent;
				iostats[cur]-> IOpending	= iobuffer->IOpending;
				iostats[cur]-> IOspent		= iobuffer->IOspent;
				iostats[cur]-> IOwspent		= iobuffer->IOwspent;

				if( init > 0){
					printf("\n");
					printf("%f MB/s\t%lu\t%lu ms\n",
						(iostats[cur]->readsector - iostats[!cur]->readsector) * 0.5 / 1024 ,
						iostats[cur]->IOpending,
						iostats[cur]->IOspent - iostats[!cur]->IOspent
						);					
				}
				init++;
				cur = !cur;
			}
			terms = strtok(NULL, "\n");
		}


		memset(buffer, '\0', 256);
		sleep(1);
	}
}
