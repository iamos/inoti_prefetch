#include <sys/inotify.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// struct inonity_event{
// 	int wd;
// 	uint32_t mask;
// 	uint32_t cookie;
// 	uint32_t len;
// 	char name[];
// };

int main(int argc, char const *argv[])
{
	char buf;
	int notifd, i, wd;

	if (argc < 2){
		printf("Usage: %s PATH [PATH...]\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	notifd = inotify_init();
	struct inotify_event* news;
	news = malloc(sizeof(struct inotify_event));
	if(news == NULL){
		perror("malloc");
		exit(EXIT_FAILURE);
	}
	wd = inotify_add_watch(notifd, argv[1], IN_CREATE | IN_DELETE | IN_ACCESS);
	if (wd < 0){
		perror("inotify_add_watch");
		exit(EXIT_FAILURE);
	}
	printf("Listening for events.\n");
	/* Get signal */
	while (1){
		read(notifd, news, sizeof(struct inotify_event));
		printf("%d\n", news->wd);
	}
	printf("Inotify stopped.\n");
	close(notifd);
	free(news);
	return 0;
}