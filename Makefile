all: 64 32

collector: sources/collector.c
	gcc -shared -fPIC sources/collector.c -o lib/collector.so -ldl

prefetch: sources/prefetch.c sources/prefetch.h
	gcc -o prefetcher sources/prefetch.c -g -pthread

tracer: sources/tracer.c sources/tracer.h
	gcc -o tracer sources/tracer.c -g

# prefetch32: sources/prefetch.c sources/prefetch.h
# 	gcc -o prefetcher32 sources/prefetch.c -g -pthread -m32

collector32: sources/collector.c
	gcc -shared -fPIC sources/collector.c -o lib/collector32.so -ldl -m32 -D BIT32

tracer32: sources/tracer.c sources/tracer.h
	gcc -o tracer32 sources/tracer.c -g -D BIT32

64: collector prefetch tracer
32: collector32 tracer32

clean: 
	rm lib/collector.so
	rm lib/collector32.so
	rm prefetcher
	rm tracer
	rm tracer32